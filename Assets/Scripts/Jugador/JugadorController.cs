using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JugadorController : MonoBehaviour
{
    private Animator animator;
    private SpriteRenderer sprite;
    private Rigidbody2D rb;

    private int estado = 0;
    private float velocidad = 10f;
    private int mon1 = 0;
    private int mon2 = 0;
    private int mon3 = 0;

    public GameObject BalaPrefrab;
    public Text puntaje;

    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        puntaje.GetComponent<Text>();
    }

    void Update()
    {
        estado = 0;
        animator.SetInteger("Estado", estado);
        rb.velocity = new Vector2(0, rb.velocity.y);
        puntaje.text = "Monedas tipo 1: " + mon1 + "\nMonedas tipo 2: " + mon2 + "\nMonedas tipo 3: " + mon3;

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            estado = 1;
            animator.SetInteger("Estado", estado);
            sprite.flipX = true;
            rb.velocity = new Vector2(velocidad * -1, rb.velocity.y);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            estado = 1;
            animator.SetInteger("Estado", estado);
            sprite.flipX = false;
            rb.velocity = new Vector2(velocidad, rb.velocity.y);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            estado = 2;
            animator.SetInteger("Estado", estado);
            rb.velocity = new Vector2(velocidad, 50);
        }

        if (Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.DownArrow))
        {
            estado = 3;
            animator.SetInteger("Estado", estado);
            sprite.flipX = false;
            rb.velocity = new Vector2(velocidad, rb.velocity.y);
        }

        if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.DownArrow))
        {
            estado = 3;
            animator.SetInteger("Estado", estado);
            sprite.flipX = true;
            rb.velocity = new Vector2(velocidad * -1, rb.velocity.y);
        }

        if (Input.GetKeyUp(KeyCode.X))
        {
            Disparar();
        }

    }

    private void Disparar()
    {
        var x = this.transform.position.x;
        var y = this.transform.position.y;

        var bala = Instantiate(BalaPrefrab, new Vector2(x, y), Quaternion.identity);

        if (sprite.flipX)
        {
            var controller = bala.GetComponent<BalaController>();
            controller.velocidad = controller.velocidad * -1;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Moneda 1")
        {
            mon1 = mon1 + 10;
        }
        if (collision.gameObject.tag == "Moneda 2")
        {
            mon2 = mon2 + 10;
        }
        if (collision.gameObject.tag == "Moneda 3")
        {
            mon3 = mon3 + 10;
        }
    }
}
