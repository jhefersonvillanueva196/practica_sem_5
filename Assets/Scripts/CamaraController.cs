using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraController : MonoBehaviour
{
    public GameObject jugador;
    private Vector3 posicion;

    void Start()
    {
        posicion = transform.position - jugador.transform.position;
    }

    void Update()
    {
        transform.position = jugador.transform.position + posicion;
    }
}
