using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoController : MonoBehaviour
{

    private Animator animator;
    private SpriteRenderer sprite;
    private Rigidbody2D rb;

    private float velocidad = 10f * -1;
    bool flip = true;

    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }


    void Update()
    {
        animator.SetInteger("Estado", 0);
        sprite.flipX = flip;
        rb.velocity = new Vector2(velocidad, rb.velocity.y);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bala")
        {
            animator.SetInteger("Estado", 1);
            Destroy(this.gameObject);
        }
        if(collision.gameObject.tag == "Limite")
        {
            if (sprite.flipX)
            {
                flip = false;
                velocidad = 10f;
            }
            else
            {
                flip = true;
                velocidad = 10f * -1;
            }
        }
    }
}
