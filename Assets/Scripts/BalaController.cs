using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BalaController : MonoBehaviour
{
    private Rigidbody2D rb;

    public Text puntos;

    private int punto = 0;

    public float velocidad = 16f;


    void Start()
    {
        puntos = GetComponent<Text>();
        rb = GetComponent<Rigidbody2D>();
        Destroy(this.gameObject, 3);
    }

    void Update()
    {
        rb.velocity = new Vector2(velocidad, rb.velocity.y);
        puntos.text = "Puntaje: " + punto;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            punto = punto + 10;
            Destroy(this.gameObject);
        }
    }

}